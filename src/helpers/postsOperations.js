import { v4 as uuidv4 } from 'uuid'

export function createPost(text, currentUser){
  return {
    id: uuidv4(),
    userId: currentUser.id,    
    user: currentUser.name,
    text: text,
    createdAt: new Date().toISOString(),
    editedAt: '',
    likesCount: new Set()
  }
}