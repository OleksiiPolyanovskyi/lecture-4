export function addLikeCountProp(posts){
  return posts.map(item => ({...item, likesCount:new Set()}))
}

export function sortPosts(posts){
  return posts.sort((a,b)=> new Date(b.createdAt) - new Date(a.createdAt))
}

export function simplifyDateFormat(posts){
  return posts.map(item => ({...item, createdAt: item.createdAt.split('T')[0]}))
}

export function changeDataFormatForBlocks(data){
  return data.reduce((acc, item)=>{
    acc[item.createdAt] = acc[item.createdAt] || []
    acc[item.createdAt].push(item)
    return acc
  }, {})
}

export function getLatestDate(posts){
  return posts[0].createdAt
}

export function participantsFormating(count){
  return `${count}`.endsWith('1') ? `${count} participant` : `${count} participants`
}

export function messagesFormating(count){
  return count === 1 ? `${count} message` : `${count} messages`
}