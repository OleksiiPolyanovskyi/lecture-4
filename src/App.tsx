import React from 'react'
import { Post, User } from './interfaces/interfaces'
import { getPostsFromServer } from './helpers/callWebApi'
import  Spinner  from './components/spinner/spinner.component'
import Home from './components/home/home.component'
import AddPost from './components/addpost/add.component'
import { addLikeCountProp, sortPosts } from './helpers/propsTransformers'
import { createCurrentUser } from './helpers/userActions'
import { createPost } from './helpers/postsOperations'
import './App.scss'

type Props = {}
type State = {
  posts:Post[]
  loading:boolean
  currentUser:User | null
}

class App extends React.Component <Props, State>{
  constructor(props:Props){
    super(props)
    this.state = {
      posts:[],
      loading:true,
      currentUser:null
    }
    this.addPost = this.addPost.bind(this)
    this.updatePost = this.updatePost.bind(this)
    this.likePost = this.likePost.bind(this)
    this.deletePost = this.deletePost.bind(this)
  }

  async componentDidMount() {
    try{
      const res = await getPostsFromServer()
      if (!res.ok) {
        throw Error(res.statusText)
      }
      const posts = await res.json()
      const sorted = sortPosts(posts)
      const withLikesCount = addLikeCountProp(sorted)
      const user = createCurrentUser()
      this.setState({
        posts:withLikesCount,
        loading:false,
        currentUser:user
      })
    }catch(error){
      console.log(error)      
    }    
  }

  addPost(text:string){
    const newPost:Post = createPost(text, this.state.currentUser)
    this.setState( prev=> ({posts:[newPost, ...prev.posts]}))
  }
  updatePost(post:Post){
    this.setState( prev => {
     const index = prev.posts.findIndex(item => item.id === post.id)
     prev.posts.splice(index, 1, post)
     return {posts:prev.posts}
    })
  }
  likePost(post:Post){
    if (post.userId === this.state.currentUser?.id) return
    this.setState( prev => {
      const index = prev.posts.findIndex(item => item.id === post.id)
      const updated = prev.posts[index]
      updated.likesCount?.add(prev.currentUser?.id)
      prev.posts.splice(index, 1, updated)
      return {posts:prev.posts}
     })
  }
  deletePost(id:string){
    this.setState(prev => ({posts: prev.posts.filter(post => post.id !== id)}))
  }
 
  render() {
    return <div className="app_container">
      {this.state.loading ? 
        <Spinner /> : 
        <div className="chat_container">
          <Home 
            posts={this.state.posts} 
            user={this.state.currentUser} 
            update={this.updatePost}
            like={this.likePost}
            deletePost={this.deletePost}
            />
          <AddPost addPost={this.addPost}/>
        </div>
      }    
    </div>
  }
}

export default App;
