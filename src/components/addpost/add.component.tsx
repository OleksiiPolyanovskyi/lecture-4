import React, { useState } from 'react'
import { Post } from '../../interfaces/interfaces'
import { Form, Button, Icon, Segment, Label } from 'semantic-ui-react'

import './add.styles.scss'

type AddPostProps = {
  post?:Post | null
  editeMode?:boolean
  addPost?(text:string):void
  changeMode?():void
  update?(post:Post | null):void
}

const AddPost:React.FC <AddPostProps> = ({
  post=null,
  editeMode = false,
  addPost,
  update,
  changeMode 
}) => {
  const initialText = post ? post.text : ''
  const [text, setText] = useState(initialText);  
 
  const handleAddPost = async () => {
    if (!text) {
      return;
    }
    if (editeMode) {
      const updated = post
      if(updated) updated.text = text
      if(update) update(updated)
      if(changeMode) changeMode();
    } else {
      if (addPost) addPost(text)
      setText('')      
    }
  };  

  return (
    <Segment className={editeMode ? "editeModeFix" : undefined} >
      <Form onSubmit={handleAddPost}>
        <Form.TextArea
          name="text"
          value={text}
          placeholder="What is the news?"
          onChange={ev => setText(ev.target.value)}
        />        
        <div className="buttonsBlock">          
          {
            !editeMode
              ? <Button floated="right" color="blue" type="submit">Post</Button>
              : (
                <Label basic size="small" as="a" floated="right" className="toolbarBtn" onClick={handleAddPost}>
                  <Icon name="save" />
                </Label>
              )
          }
        </div>
      </Form>
    </Segment>
  );
};

export default AddPost;