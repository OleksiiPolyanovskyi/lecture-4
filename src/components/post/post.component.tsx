import React, { useState } from 'react'
import { Label, Icon } from 'semantic-ui-react';
import { Post, User } from '../../interfaces/interfaces'
import AddPost from '../addpost/add.component';

import './post.styles.scss';

type PostProps = {
  post:Post
  currentUser:User | null
  update(post:Post):void
  like(post:Post):void
  deletePost(id:string):void
}

const PostComponent:React.FC<PostProps> = ({
  post,
  currentUser,
  update,
  like,
  deletePost
}) => {
  const [editeMode, setMode] = useState(false);
  const {
    id,
    userId,
    avatar,
    user,
    text,
    createdAt,
    editedAt,
    likesCount
  } = post;
  const changeMode = () => setMode(!editeMode);  
  const date = createdAt
  const selfPostClass = currentUser && userId === currentUser.id ? 'self-post' : ''
  const cardClasses = ['card', selfPostClass]  
  return (
    <div className={cardClasses.join(' ')} >
      { editeMode
        ? (
          <AddPost 
            editeMode={editeMode}
            changeMode={changeMode}
            post={post}
            update={update}         
          />
        )
        : (
          <>               
            <div className="card_content">
              <div className="meta_block">                  
                <span className="date">
                  posted by
                  {' '}
                  {user}
                  {' - '}
                  {date}
                </span>
              </div>
              <div className="description_container">
                 <div className="image_block">
                    {avatar && <img src={avatar} className="post_image"/>} 
                 </div>
                 <div className="text_block">
                    {text}
                 </div>                
              </div>
            </div>
          </>
        )}
      <div className="align-left icons_block">        
        <Label basic size="small" as="a" className="toolbarBtn" onClick={()=>like(post)}>
          <Icon name="thumbs up" />
          {likesCount?.size}
        </Label>       
        
        {currentUser && userId === currentUser.id
          ? (
            <>
              <Label basic size="small" as="a" className="toolbarBtn" onClick={changeMode}>
                <Icon name="edit" />
              </Label>
              <Label basic size="small" as="a" className="toolbarBtn" onClick={()=>deletePost(post.id)}>
                <Icon name="remove" />
              </Label>
            </>
          ) : null }
      </div>
    </div>
  );
};


export default PostComponent;